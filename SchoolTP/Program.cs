﻿using SchoolTP.DAL;
using SchoolTP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolTP
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = 0;
            int id = 1;

            while (choice != 4)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1) Display the list of students");
                Console.WriteLine("2) Add a new student");
                Console.WriteLine("3) Update a student's file");
                Console.WriteLine("4) Exit the program");

                while (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Enter a valid number.");
                }
                switch (choice)
                {
                    case 1:
                        Student.DisplayAll();
                        break;
                    case 2:
                        StudentDAL.Insert();
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("Which student's file would you like to edit ? (enter id): ");
                        while (!int.TryParse(Console.ReadLine(), out id))
                        {
                            Console.WriteLine(">Enter a valid Id:");
                        }
                        StudentDAL.UpdateById(id);
                        break;
                    default:
                        Console.WriteLine("Invalid answer. Try Again.");
                        break;
                }
            }
        }
    }
}
