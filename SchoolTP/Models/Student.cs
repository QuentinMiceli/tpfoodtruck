﻿using SchoolTP.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolTP.Models
{
    class Student
    {
        private int id;
        private string firstName;
        private string lastName;
        private DateTime birthday;
        private int weight;
        private int grade;

        public int Id { get => id; set => id = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public DateTime Birthday { get => birthday; set => birthday = value; }
        public int Weight { get => weight; set => weight = value; }
        public int Grade { get => grade; set => grade = value; }

        public Student(int id, string firstName, string lastName, DateTime birthday, int weight, int grade)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Weight = weight;
            Grade = grade;
        }

        public Student()
        {
        }

        public static void DisplayAll()
        {
            Console.Clear();
            foreach (var student in StudentDAL.SelectAll())
            {
                Console.WriteLine("Student: {0} {1} (id {2}) ", student.FirstName, student.LastName, student.Id.ToString());
                Console.WriteLine("Weight: {0}, Birthday: {1}", student.Weight.ToString(), student.Birthday.ToString("dd/MM/yyyy"));
                Console.WriteLine("Grade: {0}", student.Grade.ToString());
                Console.WriteLine("*************************************");
            }
        }
    }
}
