﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolTP.DAL
{
    public class BaseDAL
    {
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataReader reader;
        private ConnectionStringSettings connectionString;

        public SqlConnection Connection { get => connection; set => connection = value; }
        public ConnectionStringSettings ConnectionString { get => connectionString; set => connectionString = value; }
        public SqlCommand Command { get => command; set => command = value; }
        public SqlDataReader Reader { get => reader; set => reader = value; }

        public BaseDAL(string requete)
        {
            Connection = new SqlConnection();
            Command = connection.CreateCommand();
            Command.CommandText = requete;
            connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringSchool"];
            Connection.ConnectionString = connectionString.ConnectionString;
            Connection.Open();
        }

        ~BaseDAL()
        {
            Connection.Close();
        }
    }
}