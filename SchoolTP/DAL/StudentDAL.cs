﻿using SchoolTP.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolTP.DAL
{
    class StudentDAL
    {
        public static List<Student> SelectAll()
        {
            List<Student> students = new List<Student>();

            BaseDAL db = new BaseDAL("Select * from Student");
            db.Reader = db.Command.ExecuteReader();

            while (db.Reader.Read())
            {
                Student student = new Student(db.Reader.GetInt32(0), db.Reader.GetString(1), db.Reader.GetString(2), db.Reader.GetDateTime(3), db.Reader.GetInt32(4), db.Reader.GetInt32(5));
                students.Add(student);
            }

            db.Connection.Close();

            return students;
        }

        public static void Insert()
        {
            Console.Clear();
            Console.WriteLine("Please provide the student's information:");
            Console.Write(">First Name: ");
            string firstName = Console.ReadLine();
            Console.Write(">Last Name: ");
            string lastName = Console.ReadLine();
            Console.Write(">Birthday (dd/MM/yyyy): ");
            string birthdayStr = Console.ReadLine();
            DateTime birthday;
            while (!DateTime.TryParseExact(birthdayStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out birthday))
            {
                Console.Write(">Birthday (dd/MM/yyyy): ");
                birthdayStr = Console.ReadLine();
            }
            Console.Write(">Weight: ");
            var weight = 0;
            while (!int.TryParse(Console.ReadLine(), out weight))
            {
                Console.Write(">Weight: ");
            }
            Console.Write(">Grade: ");
            var grade = 0;
            while (!int.TryParse(Console.ReadLine(), out grade))
            {
                Console.Write(">Grade: ");
            }

            BaseDAL db = new BaseDAL("INSERT INTO Student (FirstName, LastName, Birthday, Weight, Grade) VALUES (@FirstNameStudent, @LastNameStudent, @BirthdayStudent, @WeightStudent, @GradeStudent)");

            db.Command.Parameters.AddWithValue("@FirstNameStudent", firstName);
            db.Command.Parameters.AddWithValue("@LastNameStudent", lastName);
            db.Command.Parameters.AddWithValue("@BirthdayStudent", birthday);
            db.Command.Parameters.AddWithValue("@WeightStudent", weight);
            db.Command.Parameters.AddWithValue("@GradeStudent", grade);

            db.Command.ExecuteNonQuery();
            db.Connection.Close();

            Console.Clear();
            Console.WriteLine("=> The student '{0} {1}' has been successfuly added to the database.", firstName, lastName);
        }

        public static void UpdateById(int id)
        {
            Console.Clear();
            BaseDAL dbCheck = new BaseDAL("SELECT COUNT(*) FROM Student WHERE ([id] = @IdStudent)");
            dbCheck.Command.Parameters.AddWithValue("@IdStudent", id);

            int StudentExist = (int)dbCheck.Command.ExecuteScalar();

            if (StudentExist > 0)
            {
                Console.WriteLine("Please provide the student's information:");
                Console.Write(">First Name: ");
                string firstName = Console.ReadLine();
                Console.Write(">Last Name: ");
                string lastName = Console.ReadLine();
                Console.Write(">Birthday (dd/MM/yyyy): ");
                string birthdayStr = Console.ReadLine();
                DateTime birthday;
                while (!DateTime.TryParseExact(birthdayStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out birthday))
                {
                    Console.WriteLine("Invalid date");
                    birthdayStr = Console.ReadLine();
                }
                Console.Write(">Weight: ");
                var weight = 0;
                while (!int.TryParse(Console.ReadLine(), out weight))
                {
                    Console.Write(">Weight: ");
                }
                Console.Write(">Grade: ");
                var grade = 0;
                while (!int.TryParse(Console.ReadLine(), out grade))
                {
                    Console.Write(">Grade: ");
                }

                BaseDAL db = new BaseDAL("UPDATE Student SET FirstName = @FirstNameStudent, LastName = @LastNameStudent, Birthday = @BirthdayStudent, Weight = @WeightStudent, Grade = @GradeStudent WHERE Id = @IdStudent");

                db.Command.Parameters.AddWithValue("@IdStudent", id);
                db.Command.Parameters.AddWithValue("@FirstNameStudent", firstName);
                db.Command.Parameters.AddWithValue("@LastNameStudent", lastName);
                db.Command.Parameters.AddWithValue("@BirthdayStudent", birthday);
                db.Command.Parameters.AddWithValue("@WeightStudent", weight);
                db.Command.Parameters.AddWithValue("@GradeStudent", grade);

                db.Command.ExecuteNonQuery();
                db.Connection.Close();
                dbCheck.Connection.Close();

                Console.Clear();
                Console.WriteLine("=> The student '{0} {1}' has been successfuly updated.", firstName, lastName);
            }
            else
            {
                Console.Clear();
                dbCheck.Connection.Close();
                Console.WriteLine("=> This student does not exist.");
            }

        }
    }
}
