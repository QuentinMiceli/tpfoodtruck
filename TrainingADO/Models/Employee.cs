﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingADO.DAL;

namespace TrainingADO.Models
{
    public class Employee
    {
        private int id;
        private string firstName;
        private string lastName;
        private DateTime birthday;

        public int Id { get => id; set => id = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public DateTime Birthday { get => birthday; set => birthday = value; }

        public Employee(int id, string firstName, string lastName, DateTime birthday)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
        }

        public Employee()
        {
        }

        public static void DisplayById(int id)
        {
            Employee employee = EmployeeDAL.SelectById(id);
            Console.WriteLine("{0} - {1} {2} : {3}", employee.Id.ToString(), employee.FirstName, employee.LastName, employee.Birthday);
        }

        public static void DisplayAll()
        {
            foreach (var employee in EmployeeDAL.SelectAll())
            {
                Console.WriteLine("{0} - {1} {2} : {3}", employee.Id.ToString(), employee.FirstName, employee.LastName, employee.Birthday);
            }
        }

    }
}
