﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingADO.Models;

namespace TrainingADO.DAL
{
    public class EmployeeDAL
    {
        public static Employee SelectById(int id)
        {
            Employee employee = new Employee();

            BaseDAL db = new BaseDAL("SELECT * FROM Employee WHERE Id = @IdEmployee");
            db.Command.Parameters.AddWithValue("@IdEmployee", id);

            using (SqlDataReader reader = db.Command.ExecuteReader())
            {
                while (reader.Read())
                {
                    employee.Id = reader.GetInt32(0);
                    employee.FirstName = reader.GetString(1);
                    employee.LastName = reader.GetString(2);
                    employee.Birthday = reader.GetDateTime(3);
                }
            }
            return employee;
        }

        public static List<Employee> SelectAll()
        {
            List<Employee> employees = new List<Employee>();

            BaseDAL db = new BaseDAL("Select * from Employee");
            db.Reader = db.Command.ExecuteReader();
            
            while (db.Reader.Read())
            {
                Employee employee = new Employee(db.Reader.GetInt32(0), db.Reader.GetString(1), db.Reader.GetString(2), db.Reader.GetDateTime(3));
                employees.Add(employee);
            }
            return employees;
        }

        public static void DeleteById(int id)
        {
            BaseDAL db = new BaseDAL("DELETE FROM Employee WHERE Id = @IdEmployee");
            db.Command.Parameters.AddWithValue("@IdEmployee", id);
            db.Command.ExecuteNonQuery();
        }

        public static void Insert()
        {
            Console.WriteLine("Enter First Name");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter Last Name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter Birthday (dd/MM/yyyy)");
            string birthdayStr = Console.ReadLine();
            DateTime birthday;
            while (!DateTime.TryParseExact(birthdayStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out birthday))
            {
                Console.WriteLine("Invalid date");
                birthdayStr = Console.ReadLine();
            }

            BaseDAL db = new BaseDAL("INSERT INTO Employee (FirstName, LastName, Birthday) VALUES (@FirstNameEmployee, @LastNameEmployee, @BirthdayEmployee)");

            db.Command.Parameters.AddWithValue("@FirstNameEmployee", firstName);
            db.Command.Parameters.AddWithValue("@LastNameEmployee", lastName);
            db.Command.Parameters.AddWithValue("@BirthdayEmployee", birthday);

            db.Command.ExecuteNonQuery();
        }

        public static void UpdateById(int id)
        {
            Console.WriteLine("Enter new First Name");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter new Last Name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter new Birthday (dd/MM/yyyy)");
            string birthdayStr = Console.ReadLine();
            DateTime birthday;
            while (!DateTime.TryParseExact(birthdayStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out birthday))
            {
                Console.WriteLine("Invalid date");
                birthdayStr = Console.ReadLine();
            }

            BaseDAL db = new BaseDAL("UPDATE Employee SET FirstName = @FirstNameEmployee, LastName = @LastNameEmployee, Birthday = @BirthdayEmployee WHERE Id = @IdEmployee");

            db.Command.Parameters.AddWithValue("@IdEmployee", id);
            db.Command.Parameters.AddWithValue("@FirstNameEmployee", firstName);
            db.Command.Parameters.AddWithValue("@LastNameEmployee", lastName);
            db.Command.Parameters.AddWithValue("@BirthdayEmployee", birthday);

            db.Command.ExecuteNonQuery();
        }

    }
}
