﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingADO.Models;
using TrainingADO.DAL;

namespace TrainingADO
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = 1;
            int id = 1;

            while (choice != 0)
            {
                Console.WriteLine("(0) Quit (1) Insert (2) UpdateById (3) DeleteById (4) DisplayById (5) DisplayAll");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        EmployeeDAL.Insert();
                        break;
                    case 2:
                        id = Convert.ToInt32(Console.ReadLine());
                        EmployeeDAL.UpdateById(id);
                        break;
                    case 3:
                        id = Convert.ToInt32(Console.ReadLine());
                        EmployeeDAL.DeleteById(id);
                        break;
                    case 4:
                        id = Convert.ToInt32(Console.ReadLine());
                        Employee.DisplayById(id);
                        break;
                    case 5:
                        Employee.DisplayAll();
                        break;
                    default:
                        Console.WriteLine("Option invalide");
                        break;
                }
            }

        }
    }
}