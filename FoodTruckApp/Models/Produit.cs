namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRODUIT")]
    public partial class PRODUIT
    {
        [Key]
        public int ID_PRODUIT { get; set; }

        public int ID_FAMILLE_REPAS { get; set; }

        [Required]
        [StringLength(30)]
        public string LIBELLE_PRODUIT { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        public int NOMBRE_DE_VENTE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PRIX { get; set; }

        [Required]
        [StringLength(255)]
        public string URL_IMAGE { get; set; }

        public int STOCK { get; set; }

        [StringLength(7)]
        public string LMMJVSD { get; set; }

        public double? MOYENNE_NOTE { get; set; }

        [StringLength(10)]
        public string UNITE { get; set; }
    }
}
