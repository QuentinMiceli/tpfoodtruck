namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UTILISATEUR")]
    public partial class UTILISATEUR 
    {
        [Key]
        public int ID_UTILISATEUR { get; set; }

        public int ID_GENRE { get; set; }

        public int ID_PROFIL { get; set; }

        public int? ID_SOCIETE { get; set; }

        [Required]
        [StringLength(30)]
        public string NOM { get; set; }

        [Required]
        [StringLength(50)]
        public string PRENOM { get; set; }

        [Required]
        [StringLength(255)]
        public string MOT_DE_PASSE { get; set; }

        [Required]
        [StringLength(100)]
        public string EMAIL { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATE_DE_NAISSANCE { get; set; }

        [StringLength(20)]
        public string TEL { get; set; }
    }
}
