namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ADRESSE")]
    public partial class ADRESSE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_ADRESSE { get; set; }

        public int ID_UTILISATEUR { get; set; }

        [Required]
        [StringLength(50)]
        public string LIBELLE_ADRESSE { get; set; }

        public bool ACTIF { get; set; }

        [Required]
        [StringLength(10)]
        public string NUMERO_RUE { get; set; }

        [Required]
        [StringLength(30)]
        public string RUE { get; set; }

        [Required]
        [StringLength(6)]
        public string CODE_POSTAL { get; set; }

        [Required]
        [StringLength(50)]
        public string VILLE { get; set; }

        [Required]
        [StringLength(30)]
        public string PAYS { get; set; }
    }
}
