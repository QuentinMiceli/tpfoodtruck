namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ALLERGENE")]
    public partial class ALLERGENE
    {
        [Key]
        public int ID_ALLERGENE { get; set; }

        [Required]
        [StringLength(255)]
        public string LIBELLE_ALLERGENE { get; set; }

        [StringLength(5)]
        public string CODE_ALLERGENE { get; set; }
    }
}
