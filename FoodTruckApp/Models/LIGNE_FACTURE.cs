namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LIGNE_FACTURE
    {
        [Key]
        public int ID_LIGNE_FACTURE { get; set; }

        public int ID_FACTURE { get; set; }

        public double QUANTITE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PRIX_UNITAIRE { get; set; }
    }
}
