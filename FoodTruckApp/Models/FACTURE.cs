namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FACTURE")]
    public partial class FACTURE
    {
        [Key]
        public int ID_FACTURE { get; set; }

        public int ID_COMMANDE { get; set; }

        public int ID_ADRESSE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TOTAL_FACTURE { get; set; }

        public bool FACTUREE { get; set; }
    }
}
