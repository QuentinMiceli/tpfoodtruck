namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LIGNE_COMMANDE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_LIGNE_COMMANDE { get; set; }

        public int ID_COMMANDE { get; set; }

        public int ID_PRODUIT { get; set; }

        public double QTE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PRIX_UNITAIRE { get; set; }
    }
}
