namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NOTE")]
    public partial class NOTE
    {
        [Key]
        public int ID_NOTE { get; set; }

        public int ID_PRODUIT { get; set; }

        public int ID_COMMANDE { get; set; }

        [Column("NOTE")]
        public double NOTE1 { get; set; }

        public DateTime DATE_NOTE { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        public bool ACTIF { get; set; }
    }
}
