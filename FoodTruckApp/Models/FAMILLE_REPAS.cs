namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FAMILLE_REPAS
    {
        [Key]
        public int ID_FAMILLE_REPAS { get; set; }

        [Required]
        [StringLength(255)]
        public string LIBELLE_FAMILLE_REPAS { get; set; }
    }
}
