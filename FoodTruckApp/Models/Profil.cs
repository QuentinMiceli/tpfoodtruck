namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PROFIL")]
    public partial class PROFIL
    {
        [Key]
        public int ID_PROFIL { get; set; }

        [Required]
        [StringLength(30)]
        public string LIBELLE_PROFIL { get; set; }
    }
}
