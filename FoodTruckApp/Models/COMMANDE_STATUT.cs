namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class COMMANDE_STATUT
    {
        [Key]
        public int ID_COMMANDE_STATUT { get; set; }

        [Required]
        [StringLength(50)]
        public string LIBELLE_COMMANDE_STATUT { get; set; }
    }
}
