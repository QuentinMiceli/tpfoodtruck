namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACTUALITES
    {
        [Key]
        public int ID_ACTUALITE { get; set; }

        [Required]
        [StringLength(255)]
        public string TITRE { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string DESCRIPTION { get; set; }

        [Required]
        [StringLength(255)]
        public string URL_IMAGE { get; set; }

        public DateTime DATE_DEBUT { get; set; }

        public DateTime DATE_FIN { get; set; }

        [Required]
        [StringLength(255)]
        public string LINK { get; set; }

        public int ID_UTILISATEUR { get; set; }

        public bool ACTIF { get; set; }
    }
}
