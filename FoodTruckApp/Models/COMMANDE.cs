namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("COMMANDE")]
    public partial class COMMANDE
    {
        [Key]
        public int ID_COMMANDE { get; set; }

        public int ID_UTILISATEUR { get; set; }

        public int ID_ADRESSE { get; set; }

        public DateTime DATE_COMMANDE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TOTAL_COMMANDE { get; set; }

        public int ID_COMMANDE_STATUS { get; set; }
    }
}
