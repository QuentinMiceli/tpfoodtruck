namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SOCIETE")]
    public partial class SOCIETE
    {
        [Key]
        public int ID_SOCIETE { get; set; }

        [Required]
        [StringLength(50)]
        public string LIBELLE_SOCIETE { get; set; }

        [Required]
        [StringLength(10)]
        public string NUMERO_RUE { get; set; }

        [Required]
        [StringLength(30)]
        public string RUE { get; set; }

        [Required]
        [StringLength(6)]
        public string CODE_POSTAL { get; set; }

        [Required]
        [StringLength(50)]
        public string VILLE { get; set; }

        [Required]
        [StringLength(30)]
        public string PAYS { get; set; }
    }
}
