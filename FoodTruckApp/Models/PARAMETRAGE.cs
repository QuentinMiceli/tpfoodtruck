namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PARAMETRAGE")]
    public partial class PARAMETRAGE
    {
        [Key]
        public int ID_PARAMETRAGE { get; set; }

        [Required]
        [StringLength(255)]
        public string PLAGE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PRIX { get; set; }
    }
}
