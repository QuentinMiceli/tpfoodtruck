namespace FoodTruckApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TYPE_REPAS
    {
        [Key]
        public int ID_TYPE_REPAS { get; set; }

        [Required]
        [StringLength(255)]
        public string LIBELLE_REPAS { get; set; }
    }
}
