﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodTruckApp.Models;
using FoodTruckApp.DAL;
using Prism.Commands;

namespace FoodTruckApp.ViewModels
{
    public class UsersViewModels : BindableBase
    {
        private ObservableCollection<UTILISATEUR> _listUsers;
        public ObservableCollection<UTILISATEUR> listUsers { get { return this._listUsers; } set { this._listUsers = value; } }

        private ObservableCollection<GENRE> _listGenre;
        public ObservableCollection<GENRE> listGENRE { get { return this._listGenre; } set { this._listGenre = value; } }


        private ObservableCollection<String> _lgenre;
        public ObservableCollection<String> lgenre { get { return this._lgenre; } set { this._lgenre = value; } }


        private ObservableCollection<UTILISATEURgenre> _luser;
        public ObservableCollection<UTILISATEURgenre> luser { get { return this._luser; } set { this._luser = value; } }


        private DelegateCommand _supprUser;
        public DelegateCommand supprUser
        {
            get { return _supprUser; }
        }



        public UsersViewModels()
        {

            this.listUsers = new ObservableCollection<UTILISATEUR>();
            this.listGENRE = new ObservableCollection<GENRE>();

            List<UTILISATEUR> collecUsers = UtilisateurDAL.SelectAll();
            collecUsers.ForEach(x => listUsers.Add(x));

            List<GENRE> collecGenre = GenreDAL.SelectAll();
            collecGenre.ForEach(x => listGENRE.Add(x));

            _supprUser = new DelegateCommand(DoSupprUser);



            ObservableCollection<String> lString = new ObservableCollection<String>();

            foreach (GENRE g in listGENRE)
            {
                lString.Add(g.LIBELLE_GENRE);
            }

            this.lgenre = lString;

            ObservableCollection<UTILISATEURgenre> listgenre = new ObservableCollection<UTILISATEURgenre>();

            foreach (UTILISATEUR u in listUsers)
            {
                UTILISATEURgenre ug = new UTILISATEURgenre(u);
                listgenre.Add(ug);
            }

            this.luser = listgenre;

        }

        protected void DoSupprUser()
        {
            
        }

        public class UTILISATEURgenre 
        {

            public UTILISATEURgenre(UTILISATEUR u)
            {
                this.ID_UTILISATEUR = u.ID_UTILISATEUR;
                this.genre = GenreDAL.SelectById(u.ID_GENRE);
                this.profil = ProfilDAL.SelectById(u.ID_PROFIL);
                this.ID_SOCIETE = u.ID_SOCIETE;
                this.NOM = u.NOM;
                this.PRENOM = u.PRENOM;
                this.MOT_DE_PASSE = u.MOT_DE_PASSE;
                this.EMAIL = u.EMAIL;
                this.DATE_DE_NAISSANCE = u.DATE_DE_NAISSANCE;
                this.TEL = u.TEL;

            }

            public int ID_UTILISATEUR { get; set; }

            public GENRE genre { get; set; }

            public PROFIL profil { get; set; }

            public int? ID_SOCIETE { get; set; }

            public string NOM { get; set; }

            public string PRENOM { get; set; }

            public string MOT_DE_PASSE { get; set; }

            public string EMAIL { get; set; }

            public DateTime DATE_DE_NAISSANCE { get; set; }

            public string TEL { get; set; }
        }


    }
}
