﻿using FoodTruckApp.DAL;
using FoodTruckApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FoodTruckApp.ViewModels
{
    class MainViewModel : BindableBase
    {
        #region Objects
        private ACTUALITES actualite;
        public ACTUALITES Actualite { get => actualite; set => SetProperty(ref actualite, value); }

        private PRODUIT produit1;
        public PRODUIT Produit1 { get => produit1; set => SetProperty(ref produit1, value); }

        private PRODUIT produit2;
        public PRODUIT Produit2 { get => produit2; set => SetProperty(ref produit2, value); }

        private PRODUIT produit3;
        public PRODUIT Produit3 { get => produit3; set => SetProperty(ref produit3, value); }
        #endregion

        #region Collections
        private ObservableCollection<ACTUALITES> listActualite;
        public ObservableCollection<ACTUALITES> ListActualite { get { return listActualite; } set { SetProperty(ref listActualite, value); } }

        private ObservableCollection<PRODUIT> listProduit;
        public ObservableCollection<PRODUIT> ListProduit { get { return listProduit; } set { SetProperty(ref listProduit, value); } }
        #endregion

        #region Delegates
        private DelegateCommand previousActu;
        private DelegateCommand nextActu;
        public DelegateCommand PreviousActu
        {
            get { return previousActu; }
        }
        
        public DelegateCommand NextActu
        {
            get { return nextActu; }
        }
        #endregion

        int counter = 1;

        public MainViewModel()
        {
            #region Actualités

            previousActu = new DelegateCommand(DoPreviousActu);
            nextActu = new DelegateCommand(DoNextActu);

            Actualite = ActualiteDAL.SelectById(counter);

            ListActualite = new ObservableCollection<ACTUALITES>();
            foreach (var a in ActualiteDAL.SelectAll())
            {
                ListActualite.Add(a);
            }

            #endregion

            #region Meilleures Ventes

            /*
            ListProduit = new ObservableCollection<PRODUIT>();
            foreach (var p in ProduitDAL.SelectAll())
            {
                ListProduit.Add(p);
            }


            Random r = new Random();
            int rInt1 = r.Next(5, ListProduit.Last().ID_PRODUIT + 1);
            int rInt2 = r.Next(5, ListProduit.Last().ID_PRODUIT + 1);
            int rInt3 = r.Next(5, ListProduit.Last().ID_PRODUIT + 1);
            */

            Produit1 = ProduitDAL.SelectById(7);
            Produit2 = ProduitDAL.SelectById(8);
            Produit3 = ProduitDAL.SelectById(9);

            #endregion

        }

        protected void DoPreviousActu()
        {
            if (counter > 1)
                Actualite = ActualiteDAL.SelectById(--counter);
        }

        protected void DoNextActu()
        {
            if (counter < ListActualite.Count)
                Actualite = ActualiteDAL.SelectById(++counter);
        }

    }
}
