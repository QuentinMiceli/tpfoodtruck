﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class ProfilDAL
    {
        public static List<PROFIL> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.PROFIL.OrderByDescending(x => x.ID_PROFIL).ToList();
            }
        }

        public static PROFIL SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.PROFIL.FirstOrDefault(x => x.ID_PROFIL == id);
            }
        }
    }
}
