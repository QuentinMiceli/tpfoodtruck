﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class AdresseDAL
    {
        public static List<ADRESSE> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.ADRESSE.OrderByDescending(x => x.ID_ADRESSE).ToList();
            }
        }

        public static ADRESSE SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.ADRESSE.FirstOrDefault(x => x.ID_ADRESSE == id);
            }
        }
    }
}
