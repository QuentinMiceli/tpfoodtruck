﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class GenreDAL
    {
        public static List<GENRE> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.GENRE.OrderByDescending(x => x.ID_GENRE).ToList();
            }
        }

        public static GENRE SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.GENRE.FirstOrDefault(x => x.ID_GENRE == id);
            }
        }
    }
}
