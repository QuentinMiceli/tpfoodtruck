﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class ActualiteDAL
    {

        public static List<ACTUALITES> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.ACTUALITES.OrderByDescending(x => x.ID_ACTUALITE).ToList();
            }
        }

        public static ACTUALITES SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.ACTUALITES.FirstOrDefault(x => x.ID_ACTUALITE == id);
            }
        }

    }
}