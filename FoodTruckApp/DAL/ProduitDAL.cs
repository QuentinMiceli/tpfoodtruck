﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class ProduitDAL
    {
        public static List<PRODUIT> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.PRODUIT.OrderByDescending(x => x.ID_PRODUIT).ToList();
            }
        }

        public static PRODUIT SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.PRODUIT.FirstOrDefault(x => x.ID_PRODUIT == id);
            }
        }

        public static PRODUIT Insert(PRODUIT _prod)
        {
            using (var context = new BaseModel())
            {
                context.PRODUIT.Add(_prod);
                context.SaveChanges();

                return context.PRODUIT.FirstOrDefault(
                    x => (x.LIBELLE_PRODUIT.ToLower() == _prod.LIBELLE_PRODUIT.ToLower()));
            }
        }

        public static void DeleteById(int _id)
        {
            using (var context = new BaseModel())
            {
                PRODUIT prod = context.PRODUIT.FirstOrDefault(x => x.ID_PRODUIT == _id);
                if (prod != null)
                {
                    context.PRODUIT.Remove(prod);
                    context.SaveChanges();
                }
            }
        }

        public static PRODUIT UpdateById(int _id, PRODUIT _updatedProd)
        {
            using (var context = new BaseModel())
            {
                PRODUIT initialProd = context.PRODUIT.SingleOrDefault(x => x.ID_PRODUIT == _id);
                if (initialProd != null && _updatedProd != null)
                {
                    initialProd.LIBELLE_PRODUIT = _updatedProd.LIBELLE_PRODUIT;
                    initialProd.DESCRIPTION = _updatedProd.DESCRIPTION;
                    initialProd.PRIX = _updatedProd.PRIX;
                    initialProd.URL_IMAGE = _updatedProd.URL_IMAGE;
                    initialProd.LMMJVSD = _updatedProd.LMMJVSD;
                    initialProd.UNITE = _updatedProd.UNITE;
                    initialProd.ID_FAMILLE_REPAS = _updatedProd.ID_FAMILLE_REPAS;

                    context.SaveChanges();
                }
                return context.PRODUIT.FirstOrDefault(x => x.ID_PRODUIT == _id);
            }
        }

    }
}
