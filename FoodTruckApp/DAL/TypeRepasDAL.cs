﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class TypeRepasDAL
    {
        public static List<TYPE_REPAS> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.TYPE_REPAS.OrderByDescending(x => x.ID_TYPE_REPAS).ToList();
            }
        }

        public static TYPE_REPAS SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.TYPE_REPAS.FirstOrDefault(x => x.ID_TYPE_REPAS == id);
            }
        }
    }
}
