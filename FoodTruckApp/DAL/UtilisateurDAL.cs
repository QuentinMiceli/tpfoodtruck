﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class UtilisateurDAL
    {
        public static List<UTILISATEUR> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.UTILISATEUR.OrderByDescending(x => x.ID_UTILISATEUR).ToList();
            }
        }

        public static UTILISATEUR SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.UTILISATEUR.FirstOrDefault(x => x.ID_UTILISATEUR == id);
            }
        }

        public static UTILISATEUR Insert(UTILISATEUR _usr)
        {
            using (var context = new BaseModel())
            {
                context.UTILISATEUR.Add(_usr);
                context.SaveChanges();

                return context.UTILISATEUR.FirstOrDefault(
                    x => (x.EMAIL.ToLower() == _usr.EMAIL.ToLower()));
            }
        }

        public static void DeleteById(int _id)
        {
            using (var context = new BaseModel())
            {
                UTILISATEUR usr = context.UTILISATEUR.FirstOrDefault(x => x.ID_UTILISATEUR == _id);
                if (usr != null)
                {
                    context.UTILISATEUR.Remove(usr);
                    context.SaveChanges();
                }
            }
        }

        public static UTILISATEUR UpdateById(int _id, UTILISATEUR _updatedUsr)
        {
            using (var context = new BaseModel())
            {
                UTILISATEUR initialUsr = context.UTILISATEUR.SingleOrDefault(x => x.ID_UTILISATEUR == _id);
                if (initialUsr != null && _updatedUsr != null)
                {
                    initialUsr.NOM = _updatedUsr.NOM;
                    initialUsr.PRENOM = _updatedUsr.PRENOM;
                    initialUsr.MOT_DE_PASSE = _updatedUsr.MOT_DE_PASSE;
                    initialUsr.EMAIL = _updatedUsr.EMAIL;
                    initialUsr.DATE_DE_NAISSANCE = _updatedUsr.DATE_DE_NAISSANCE;
                    initialUsr.TEL = _updatedUsr.TEL;
                    initialUsr.ID_GENRE = _updatedUsr.ID_GENRE;
                    initialUsr.ID_SOCIETE = _updatedUsr.ID_SOCIETE;
                    initialUsr.ID_UTILISATEUR = _updatedUsr.ID_UTILISATEUR;

                    context.SaveChanges();
                }
                return context.UTILISATEUR.FirstOrDefault(x => x.ID_UTILISATEUR == _id);
            }
        }

    }
}
