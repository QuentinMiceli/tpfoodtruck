﻿using FoodTruckApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckApp.DAL
{
    class FamilleRepasDAL
    {
        public static List<FAMILLE_REPAS> SelectAll()
        {
            using (var context = new BaseModel())
            {
                return context.FAMILLE_REPAS.OrderByDescending(x => x.ID_FAMILLE_REPAS).ToList();
            }
        }

        public static FAMILLE_REPAS SelectById(int id)
        {
            using (var context = new BaseModel())
            {
                return context.FAMILLE_REPAS.FirstOrDefault(x => x.ID_FAMILLE_REPAS == id);
            }
        }
    }
}
